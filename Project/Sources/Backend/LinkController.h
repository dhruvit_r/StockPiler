//
//  LinkController.h
//  StockPiler
//
//  Created by heavenly-awker on 01/01/14.
//  Copyright (c) 2014 tuck. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LinkController : NSObject

- (NSString*)linkPathForAppWithTitle:(NSString*)title;

- (BOOL)addLinkForAppWithTitle:(NSString*)title;
- (BOOL)removeLinkForAppWithTitle:(NSString*)title;

- (BOOL)isThereALinkForAppWithTitle:(NSString*)title;

- (void)checkForCompletionOfDownload;

- (NSDictionary*)allRegisteredLinks;

+ (id)sharedLinker;

@end
