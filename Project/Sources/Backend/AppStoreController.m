//
//  AppStoreController.m
//  StockPiler
//
//  Created by heavenly-awker on 23/12/13.
//  Copyright (c) 2013 tuck. All rights reserved.
//

#import "AppStoreController.h"

@interface AppStoreController () {
    NSString *appStoreTemporaryLocation;
}

@property (nonatomic, strong) NSString *AppStoreTemporaryLocation;

@end

@implementation AppStoreController

@synthesize AppStoreTemporaryLocation = appStoreTemporaryLocation;

- (NSArray*)infoArray {
    NSString *appStoreTemporaryDirectory = [self appStoreTemporaryFolder];
    NSDictionary *manifest = [NSDictionary dictionaryWithContentsOfFile:[appStoreTemporaryDirectory stringByAppendingString:@"/manifest.plist"]];
    NSMutableArray *appsArray = [NSMutableArray array];
    for (NSDictionary *appManifest in [manifest objectForKey:@"representations"]) {
        NSMutableDictionary *appDictionary = [NSMutableDictionary dictionary];
        NSArray *assets = [appManifest valueForKey:@"assets"];
        for (NSDictionary *dict in assets) {
            //Adding name
            [appDictionary setObject:[dict valueForKey:@"name"] forKey:@"name"];
            //Adding size
            [appDictionary setObject:[dict valueForKey:@"size"] forKey:@"size"];
        }
        //Add path
        [appDictionary setObject:[NSString stringWithFormat:@"%@/%@/%@", appStoreTemporaryDirectory, [appManifest valueForKey:@"item-id"], [appDictionary valueForKey:@"name"]] forKey:@"path"];
        //Add version
        [appDictionary setObject:[appManifest valueForKey:@"bundle-version"] forKey:@"version"];
        //Add title
        [appDictionary setObject:[appManifest valueForKey:@"title"] forKey:@"title"];
        //Add subtitle
        [appDictionary setObject:[appManifest valueForKey:@"subtitle"] forKey:@"subtitle"];
        //Add image
        NSString *appImagePath = [[[appDictionary objectForKey:@"path"] stringByDeletingLastPathComponent] stringByAppendingString:@"/flyingIcon"];
        NSImage *image;
        image = [[NSImage alloc] initWithContentsOfFile:appImagePath];
        if (!image) {
            image = [[NSWorkspace sharedWorkspace] iconForFileType:@"app"];
        }
        [appDictionary setObject:image forKey:@"image"];
        
        //Add app to main dictionary
        [appsArray addObject:appDictionary];
    }
    return appsArray;
    
}

- (NSDictionary*)infoDictionary {
    NSString *appStoreTemporaryDirectory = [self appStoreTemporaryFolder];
    NSDictionary *manifest = [NSDictionary dictionaryWithContentsOfFile:[appStoreTemporaryDirectory stringByAppendingString:@"/manifest.plist"]];
    NSMutableDictionary *appsDictionary = [NSMutableDictionary dictionary];
    for (NSDictionary *appManifest in [manifest objectForKey:@"representations"]) {
        NSMutableDictionary *appDictionary = [NSMutableDictionary dictionary];
        NSArray *assets = [appManifest valueForKey:@"assets"];
        for (NSDictionary *dict in assets) {
            //Adding name
            [appDictionary setObject:[dict valueForKey:@"name"] forKey:@"name"];
            //Adding size
            [appDictionary setObject:[dict valueForKey:@"size"] forKey:@"size"];
        }
        //Add path
        [appDictionary setObject:[NSString stringWithFormat:@"%@/%@/%@", appStoreTemporaryDirectory, [appManifest valueForKey:@"item-id"], [appDictionary valueForKey:@"name"]] forKey:@"path"];
        //Add version
        [appDictionary setObject:[appManifest valueForKey:@"bundle-version"] forKey:@"version"];
        //Add title
        [appDictionary setObject:[appManifest valueForKey:@"title"] forKey:@"title"];
        //Add subtitle
        [appDictionary setObject:[appManifest valueForKey:@"subtitle"] forKey:@"subtitle"];
        //Add image
        //Add image
        NSString *appImagePath = [[[appDictionary objectForKey:@"path"] stringByDeletingLastPathComponent] stringByAppendingString:@"/flyingIcon"];
        NSImage *image;
        image = [[NSImage alloc] initWithContentsOfFile:appImagePath];
        if (!image) {
            image = [[NSWorkspace sharedWorkspace] iconForFileType:@"app"];
        }
        [appDictionary setObject:image forKey:@"image"];
        
        //Add app to main dictionary
        [appsDictionary setObject:appDictionary forKey:[appDictionary valueForKey:@"title"]];
    }
    return appsDictionary;
}

+ (AppStoreController *)sharedAppStoreController {
    static AppStoreController *appStoreController;
    
    @synchronized(self) {
        if (!appStoreController) {
            appStoreController = [[AppStoreController alloc] init];
        }
        return appStoreController;
    }
}

- (NSString*)appStoreTemporaryFolder {
    if (appStoreTemporaryLocation) {
        return appStoreTemporaryLocation;
    }
    NSString *appStoreDirectory = [[NSTemporaryDirectory() stringByDeletingLastPathComponent] stringByAppendingString:@"/C/com.apple.appstore"];
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    BOOL isDir;
    if ([defaultManager fileExistsAtPath:appStoreDirectory isDirectory:&isDir] && (isDir)) {
        NSString *probableManifest = [appStoreDirectory stringByAppendingString:@"/manifest.plist"];
        BOOL exists = [defaultManager fileExistsAtPath:probableManifest];
        if (exists) {
            appStoreTemporaryLocation = appStoreDirectory;
            return appStoreDirectory;
        }
    }
    return nil;
}

@end
