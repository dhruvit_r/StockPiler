//
//  AppStoreController.h
//  StockPiler
//
//  Created by heavenly-awker on 23/12/13.
//  Copyright (c) 2013 tuck. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppStoreController : NSObject

+ (AppStoreController *)sharedAppStoreController;

- (NSArray*)infoArray;
- (NSDictionary*)infoDictionary;

- (NSString*)appStoreTemporaryFolder;

@end
