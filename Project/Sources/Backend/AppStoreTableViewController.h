//
//  AppStoreTableViewController.h
//  StockPiler
//
//  Created by heavenly-awker on 25/12/13.
//  Copyright (c) 2013 tuck. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "AppStoreCellController.h"
#import "AppStoreController.h"

@interface AppStoreTableViewController : NSViewController <NSTableViewDataSource, NSTableViewDelegate> {
    NSWindow *parentWindow;
    NSTableView *table;
    NSMutableArray *currentInfo;
    AppStoreController *storeController;
    NSScrollView *scrollView;
    NSTimer *timer;
    NSNumber *isEmpty;
    int CELL_HEIGHT;
    int CELL_WIDTH;
}

@property (nonatomic, strong) IBOutlet NSWindow *ParentWindow;
@property (nonatomic, strong) IBOutlet NSTableView *Table;
@property (nonatomic, strong) IBOutlet NSScrollView *ScrollView;

- (void)updateInfo;
- (void)resizeContents;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;
- (void)awakeFromNib;
- (void)updateTable;

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView;
- (CGFloat)tableView:(NSTableView *)tableView heightOfRow:(NSInteger)row;

-(NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row;




@end
