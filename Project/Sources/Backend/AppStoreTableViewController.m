#import "AppStoreTableViewController.h"

@interface AppStoreTableViewController ()

- (NSView *)emptyView;
- (void)removeDuplicateEntries;

@end

@implementation AppStoreTableViewController

@synthesize Table = table;
@synthesize ParentWindow = parentWindow;

int CELL_WIDTH = 350;
int CELL_HEIGHT = 70;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:@"AppStoreTableView" bundle:[NSBundle mainBundle]];
    if (self) {
        isEmpty = [NSNumber numberWithBool:NO];
        [self updateInfo];
        [table setDelegate:self];
        [table setDataSource:self];
        [table reloadData];
        [self resizeContents];
        [parentWindow makeKeyAndOrderFront:self];
        timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                 target:self
                                               selector:@selector(removeDuplicateEntries)
                                               userInfo:nil
                                                repeats:YES];
    }
    return self;
}

- (void)removeDuplicateEntries {
    for (int i = 0; i < [table numberOfRows]; i++) {
        AppStoreCellController *cellConroller = [table viewAtColumn:0 row:i makeIfNecessary:NO];
        if (![[NSFileManager defaultManager] fileExistsAtPath:[[cellConroller InfoDictionary] objectForKey:@"path"]]) {
            [table removeRowsAtIndexes:[NSIndexSet indexSetWithIndex:i] withAnimation:NSTableViewAnimationEffectFade];
            CGRect frame = self.view.frame;
            [table sizeToFit];
            [[table enclosingScrollView] setFrameSize:table.frame.size];
            [[table enclosingScrollView] setNeedsDisplay:YES];
            CGFloat h = frame.size.height;
            int i = h;
            i += i;
        }
    }
}

- (void)resizeContents {
    CGRect frame = self.view.frame;
    [table sizeToFit];
    [[table enclosingScrollView] setFrameSize:table.frame.size];
    CGFloat h = frame.size.height;
    int i = h;
    i += i;
}

- (void)awakeFromNib {
}

- (void)updateTable {
    [self updateInfo];
    [[self Table] reloadData];
    [self resizeContents];
}

- (void)updateInfo {
    if (!currentInfo) {
        currentInfo = [[NSMutableArray alloc] init];
    } else {
        [currentInfo removeAllObjects];
    }
    if (!storeController) {
        storeController = [AppStoreController sharedAppStoreController];
    }
    [storeController infoDictionary];
    
    for (id object in [storeController infoArray]) {
        if (![currentInfo containsObject:object]) {
            if ([[NSFileManager defaultManager] fileExistsAtPath:[object valueForKey:@"path"]]) {
                [currentInfo addObject:object];
            }
        }
    }
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    [self updateInfo];
    unsigned long count = (unsigned long)[currentInfo count];
    if (count == 0) {
        count += 1;
        isEmpty = [NSNumber numberWithBool:YES];
    }
    return count;
}

- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row {
    return NO;
}

-(CGFloat)tableView:(NSTableView *)tableView heightOfRow:(NSInteger)row {
    return 70.0f;
}

-(NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
    if ([isEmpty boolValue]) {
        isEmpty = [NSNumber numberWithBool:NO];
        return [self emptyView];
    }
    AppStoreCellController *cell = [AppStoreCellController cellFromNibWithInfo:[currentInfo objectAtIndex:row]];
    return cell;
}

- (NSView *)emptyView {
    NSRect viewRect = NSMakeRect(0, 0, 360, 70);
    NSView *view = [[NSView alloc] initWithFrame:viewRect];
    NSRect labelRect = NSMakeRect(88, 27, 185, 17);
    NSTextField *label = [[NSTextField alloc] initWithFrame:labelRect];
    [label setBordered:NO];
    [label setBezeled:NO];
    [label setStringValue:@"You're not downloading anything."];
    [label setFont:[NSFont fontWithName:[[label font] fontName] size:11]];
    [label setTextColor:[NSColor grayColor]];
    [view addSubview:label positioned:NSWindowAbove relativeTo:nil];
    return view;
}

@end
