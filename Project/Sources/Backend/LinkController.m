//
//  LinkController.m
//  StockPiler
//
//  Created by heavenly-awker on 01/01/14.
//  Copyright (c) 2014 tuck. All rights reserved.
//

#import "LinkController.h"
#import "AppStoreController.h"

@interface LinkController ()

@property (nonatomic, strong) NSMutableDictionary *linkDictionary;
@property (nonatomic, strong) NSString *appManifestPath;
@property (nonatomic, strong) NSString *appCachePath;

- (NSString*)pathForLinkOfPackageAtPath:(NSString*)path withTitle:(NSString*)title;

@end

@implementation LinkController

@synthesize linkDictionary;
@synthesize appCachePath;
@synthesize appManifestPath;

+ (id)sharedLinker {
    static LinkController *linkController;
    
    @synchronized(self) {
        if (!linkController) {
            linkController = [[LinkController alloc] init];
        }
        if (linkController) {
            [linkController setAppCachePath:[[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]
                                             stringByAppendingString:@"/com.tuck.StockPiler"]];
            if (![linkController appCachePath]) {
                NSLog(@"WARNING 9006:No cache directory");
                return nil;
            }
            
            BOOL isDir;
            
            //Check if cache folder exists
            if (![[NSFileManager defaultManager] fileExistsAtPath:[linkController appCachePath] isDirectory:&isDir] || !isDir) {
                //File exists, but not a directory (Need to be directory)
                if (!isDir) {
                    NSError *error;
                    //Remove pre-existing file
                    BOOL succeed = [[NSFileManager defaultManager] removeItemAtPath:[linkController appCachePath] error:&error];
                    if (!succeed) {
                        NSLog(@"WARNING 9007:Unable to delete file:%@ with WARNING:%@", [linkController appCachePath], error);
                    }
                    //Create new directory
                    succeed = [[NSFileManager defaultManager] createDirectoryAtPath:[linkController appCachePath]
                                                        withIntermediateDirectories:YES
                                                                         attributes:nil
                                                                              error:&error];
                    if (!succeed) {
                        NSLog(@"WARNING 9008:Unable to create directory:%@ with WARNING:%@", [linkController appCachePath], error);
                    }
                    //File Doesn't exist
                } else {
                    //Create new directory
                    NSError *error;
                    BOOL succeed = [[NSFileManager defaultManager] createDirectoryAtPath:[linkController appCachePath]
                                                             withIntermediateDirectories:YES
                                                                              attributes:nil
                                                                                   error:&error];
                    if (!succeed) {
                        NSLog(@"WARNING 9009:Unable to create directory:%@ with WARNING:%@", [linkController appCachePath], error);
                    }
                }
            }
            
            //Check if manifest.plist (of StockPiler) exists
            [linkController setAppManifestPath:[[linkController appCachePath] stringByAppendingString:@"/manifest.plist"]];
            if ([[NSFileManager defaultManager] fileExistsAtPath:[linkController appManifestPath] isDirectory:&isDir] && !isDir) {
                [linkController setLinkDictionary:[NSMutableDictionary dictionaryWithContentsOfFile:[linkController appManifestPath]]];
            } else {
                [linkController setLinkDictionary:[NSMutableDictionary dictionary]];
            }
        }
        return linkController;
    }
}

- (BOOL)isThereALinkForAppWithTitle:(NSString*)title {
    NSString *linkPath = [linkDictionary valueForKey:title];
    if (!linkPath) {
        return NO;
    }
    return YES;
}

- (NSString*)linkPathForAppWithTitle:(NSString*)title {
    if (!linkDictionary) {
        NSLog(@"WARNING 9010:No dictionary");
        [linkDictionary writeToFile:appManifestPath atomically:YES];
        return nil;
    }
    NSString *linkPath = [linkDictionary valueForKey:title];
    if (!linkPath) {
        [linkDictionary writeToFile:appManifestPath atomically:YES];
        NSLog(@"WARNING 9011:No link info for this app");
    }
    [linkDictionary writeToFile:appManifestPath atomically:YES];
    return linkPath;
}

- (NSString*)pathForLinkOfPackageAtPath:(NSString*)path withTitle:(NSString *)title {
    if (appCachePath)
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL isDir;
        BOOL succeed;
        NSError *error;
        NSString *fileDestination = [NSString stringWithFormat:@"%@/%@.pkg", appCachePath, title];
        //A file exists at cache path. Should be a directory. File will be deleted.
        if ([fileManager fileExistsAtPath:fileDestination isDirectory:&isDir] && !isDir) {
            succeed = [fileManager removeItemAtPath:fileDestination error:&error];
            if (!succeed) {
                NSLog(@"%@", error);
                return nil;
            }
        }
        //No link, file or directory with the name of the to-be link exists.
        if (![fileManager fileExistsAtPath:fileDestination]) {
            //A cache file exists instead of directory. It is deleted. Cache directory is created. Package is linked.
            if ([fileManager fileExistsAtPath:appCachePath isDirectory:&isDir] && !isDir) {
                [fileManager removeItemAtPath:appCachePath error:nil];
                [fileManager createDirectoryAtPath:appCachePath
                       withIntermediateDirectories:YES
                                        attributes:nil
                                             error:nil];
                succeed = [fileManager linkItemAtPath:path toPath:fileDestination error:&error];
                if (!succeed) {
                    NSLog(@"%@", error);
                    return nil;
                } else {
                    return fileDestination;
                }
            //Nothing exists as cache. Cache directory is created. Package is linked.
            } else if (![fileManager fileExistsAtPath:appCachePath]) {
                [fileManager createDirectoryAtPath:appCachePath
                       withIntermediateDirectories:YES
                                        attributes:nil
                                             error:nil];
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *fileDestination = [NSString stringWithFormat:@"%@/%@.pkg", appCachePath, title];
                NSError *error;
                succeed = [fileManager linkItemAtPath:path toPath:fileDestination error:&error];
                if (!succeed) {
                    NSLog(@"%@", error);
                    return nil;
                } else {
                    return fileDestination;
                }
            //Cache directory already exists. Package will be linked.
            } else if ([fileManager fileExistsAtPath:appCachePath isDirectory:&isDir] && isDir) {
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *fileDestination = [NSString stringWithFormat:@"%@/%@.pkg", appCachePath, title];
                NSError *error;
                succeed = [fileManager linkItemAtPath:path toPath:fileDestination error:&error];
                if (!succeed) {
                    NSLog(@"%@", error);
                    return nil;
                } else {
                    return fileDestination;
                }
            }
        };
    }
    return nil;
}

- (NSDictionary*)allRegisteredLinks {
    if (!linkDictionary) {
        NSLog(@"WARNING 9023:No dictionary exists");
        [linkDictionary writeToFile:appManifestPath atomically:YES];
        return nil;
    }
    return linkDictionary;
}

- (BOOL)addLinkForAppWithTitle:(NSString*)title {
    BOOL isDir = NO;
    if (!linkDictionary) {
        linkDictionary = [NSMutableDictionary dictionary];
        if (!linkDictionary) {
            NSLog(@"WARNING 9012:No dictionary exists. Tried but failed to create one");
        }
        [linkDictionary writeToFile:appManifestPath atomically:YES];;
    }
    if (!title) {
        NSLog(@"WARNING 9013:No title specified");
        return NO;
    }
    NSString *packagePath = [[[[AppStoreController sharedAppStoreController] infoDictionary] valueForKey:title] valueForKey:@"path"];
    if (!packagePath) {
        NSLog(@"WARNING 9014:Unable to find package path");
        [linkDictionary writeToFile:appManifestPath atomically:YES];
        return NO;
    }
    if (![[NSFileManager defaultManager] fileExistsAtPath:packagePath isDirectory:&isDir] || isDir) {
        NSLog(@"WARNING 9015:Package doesn't exist or is directory");
        [linkDictionary writeToFile:appManifestPath atomically:YES];
        return NO;
    }
    NSString *linkPath = [self pathForLinkOfPackageAtPath:packagePath withTitle:title];
    if (![[NSFileManager defaultManager] fileExistsAtPath:linkPath isDirectory:&isDir] || isDir) {
        NSLog(@"WARNING 9016:Link doesn't exist or is directory");
        [linkDictionary writeToFile:appManifestPath atomically:YES];
        return NO;
    }
    if (!linkDictionary) {
        NSLog(@"WARNING 9017:Dictionary doesn't exist. Will try to create one");
        linkDictionary = [NSMutableDictionary dictionary];
        if (!linkDictionary) {
            NSLog(@"WARNING 9018:Failed to create dictionary");
            [linkDictionary writeToFile:appManifestPath atomically:YES];
            return NO;
        }
    }
    if ([linkDictionary valueForKey:title] != nil) {
        NSLog(@"Value: %@\n", [linkDictionary valueForKey:title]);
        [linkDictionary writeToFile:appManifestPath atomically:YES];
        NSLog(@"WARNING 9005:Link already exists");
    }
    [linkDictionary setObject:linkPath forKey:title];
    if (![linkDictionary valueForKey:title]) {
        [linkDictionary writeToFile:appManifestPath atomically:YES];
        NSLog(@"WARNING 9019:Failed adding entry to dictionary");
        return NO;
    }
    [linkDictionary writeToFile:appManifestPath atomically:YES];
    return YES;
}
- (BOOL)removeLinkForAppWithTitle:(NSString*)title {
    if (!linkDictionary) {
        NSLog(@"WARNING 9020:No dictionary exists");
        [linkDictionary writeToFile:appManifestPath atomically:YES];
        return NO;
    }
    NSError *error;
    BOOL succeed = [[NSFileManager defaultManager] removeItemAtPath:[linkDictionary valueForKey:title]  error:&error];
    if (!succeed) {
        NSLog(@"%@", error);
        [linkDictionary writeToFile:appManifestPath atomically:YES];
        return NO;
    }
    [linkDictionary removeObjectForKey:title];
    if ([linkDictionary valueForKey:title]) {
        NSLog(@"WARNING 9021:Unable to remove link entry. Retrying");
        [linkDictionary removeObjectForKey:title];
        if ([linkDictionary valueForKey:title]) {
            NSLog(@"WARNING 9022:Yet unable to remove link entry");
            [linkDictionary writeToFile:appManifestPath atomically:YES];
            return NO;
        }
    }
    [linkDictionary writeToFile:appManifestPath atomically:YES];
    return YES;
}

- (void)checkForCompletionOfDownload {
    for (id appTitle in [linkDictionary allKeys]) {
        NSString *appLink = [linkDictionary objectForKey:appTitle];
        if (!appLink) {
            NSLog(@"WARNING 9024:Unable to find app title");
        }
        NSString *packagePath = [[[[AppStoreController sharedAppStoreController] infoDictionary] valueForKey:appTitle] valueForKey:@"path"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:packagePath] || !packagePath) {
            NSString *desktopPath = [NSString stringWithFormat:@"%@/%@.pkg", [NSSearchPathForDirectoriesInDomains(NSDesktopDirectory, NSUserDomainMask, YES) objectAtIndex:0], appTitle];
            NSError *error;
            [[NSFileManager defaultManager] copyItemAtPath:[[self allRegisteredLinks] valueForKey:appTitle] toPath:desktopPath error:&error];
            if (error) {
                NSLog(@"%@", error);
            }
            [self removeLinkForAppWithTitle:appTitle];
        }
    }
}

@end
