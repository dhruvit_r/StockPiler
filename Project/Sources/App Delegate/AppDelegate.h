//
//  AppDelegate.h
//  StockPiler
//
//  Created by heavenly-awker on 22/12/13.
//  Copyright (c) 2013 tuck. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MenuItem.h"
#import "AppStoreTableViewController.h"

@class MenuItem;

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (nonatomic, strong) MenuItem *menuItem;
@property (nonatomic, strong) NSStatusItem *statusItem;

@end
