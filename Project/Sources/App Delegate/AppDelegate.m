//
//  AppDelegate.m
//  StockPiler
//
//  Created by heavenly-awker on 22/12/13.
//  Copyright (c) 2013 tuck. All rights reserved.
//

#import "AppDelegate.h"
#import "LinkController.h"

@implementation AppDelegate

@synthesize menuItem;
@synthesize statusItem;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    CGFloat thickness = [[NSStatusBar systemStatusBar] thickness];
    self.statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:thickness];
    menuItem = [[MenuItem alloc] initWithFrame:(NSRect){.size={thickness, thickness}}]; /* square item */
    [[self statusItem] setView:menuItem];
    
    NSTimer *timer;
    timer = [NSTimer scheduledTimerWithTimeInterval:1
                                             target:[LinkController sharedLinker]
                                           selector:@selector(checkForCompletionOfDownload)
                                           userInfo:nil
                                            repeats:YES];
}

@end
