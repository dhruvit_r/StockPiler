//
//  DownloadProgress.h
//  StockPiler
//
//  Created by heavenly-awker on 29/01/14.
//  Copyright (c) 2014 tuck. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DownloadProgress : NSView {
    double progressValue;
}

- (void)incrementBy:(double)doubleValue;
- (double)doubleValue;
- (void)setProgressValue:(double)doubleValue;

@end
