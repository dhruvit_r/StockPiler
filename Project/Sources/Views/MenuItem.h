//
//  MenuItem.h
//  StockPiler
//
//  Created by heavenly-awker on 22/12/13.
//  Copyright (c) 2013 tuck. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "AppDelegate.h"
#import "AppStoreTableViewController.h"

@interface MenuItem : NSView

@property (nonatomic, strong) NSImage *image;
@property (nonatomic, strong) NSImage *alternateImage;
@property (nonatomic, strong) NSImage *title;
@property (nonatomic, assign, getter = isActive) BOOL active;
@property (nonatomic, strong) NSPopover *popover;
@property (nonatomic, strong) id popoverTransiencyMonitor;

- (BOOL)isActive;

@end
