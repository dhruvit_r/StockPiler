//
//  MenuItem.m
//  StockPiler
//
//  Created by heavenly-awker on 22/12/13.
//  Copyright (c) 2013 tuck. All rights reserved.
//

#import "MenuItem.h"

static void *kActiveChangedKVO = &kActiveChangedKVO;

@implementation MenuItem

@synthesize image;
@synthesize alternateImage;
@synthesize title;
@synthesize popover;
@synthesize active;


- (void)drawRect:(NSRect)rect {
    rect = CGRectInset(rect, 0, 0);
    NSImage *menuItemImage;
    if ([self isActive]) {
        menuItemImage = [NSImage imageNamed:@"StockPilerStatusSelected"];
        [[NSColor selectedMenuItemColor] set];
        NSRectFill(rect);
    } else {
        menuItemImage = [NSImage imageNamed:@"StockPilerStatus"];
        [[NSColor clearColor] set];
        NSRectFillUsingOperation(rect, NSCompositeSourceOver);
    }
    [menuItemImage drawInRect:NSInsetRect(rect, 2, 2)
                      fromRect:NSZeroRect
                     operation:NSCompositeSourceOver
                      fraction:1.0];
}

- (void)mouseDown:(NSEvent *)theEvent {
    self.active = !self.active;
    if (self.active) {
        [self setupPopover];
        [self.popover showRelativeToRect:[self frame]
                                  ofView:self
                           preferredEdge:NSMinYEdge];
    } else {
        [self.popover performClose:self];
        self.popover = nil;
        if (self.popoverTransiencyMonitor != nil) {
            [NSEvent removeMonitor:self.popoverTransiencyMonitor];
            self.popoverTransiencyMonitor = nil;
        }
    }
    [self setNeedsDisplay:YES];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (context == kActiveChangedKVO) {
        [self setNeedsDisplay:YES];
    }
}

- (void)setupPopover {
    if (!self.popover) {
        self.popover = [[NSPopover alloc] init];
        [self.popover setContentViewController:[[AppStoreTableViewController alloc] initWithNibName:@"AppStoreTableView" bundle:[NSBundle mainBundle]]];
        if (self.popoverTransiencyMonitor == nil) {
            self.popoverTransiencyMonitor = [NSEvent addGlobalMonitorForEventsMatchingMask:NSLeftMouseDownMask | NSRightMouseDownMask handler:^(NSEvent* event){
                NSPoint mouseClickLocation = [NSEvent mouseLocation];
                BOOL isInRect = NSPointInRect(mouseClickLocation, [self frame]);
                if (!isInRect) {
                    [self mouseDown:event];
                }
            }];
        }
    }
}
    

@end