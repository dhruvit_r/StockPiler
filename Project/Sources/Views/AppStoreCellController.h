//
//  AppStoreCellController.h
//  StockPiler
//
//  Created by heavenly-awker on 23/12/13.
//  Copyright (c) 2013 tuck. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DownloadProgress.h"

@interface AppStoreCellController : NSView {
@private
    NSButton *saveApp;
    DownloadProgress *downloadProgress;
    NSDictionary *infoDictionary;
    NSPopover *popover;
    NSTextField *titleField;
    NSTextField *subtitleField;
    NSImageView *imageView;
}

@property (nonatomic, strong) IBOutlet NSImageView *ImageView;
@property (nonatomic, strong) IBOutlet NSTextField *TitleField;
@property (nonatomic, strong) IBOutlet NSButton *SaveApp;
@property (nonatomic, strong) IBOutlet DownloadProgress *DownloadProgress;
@property (nonatomic, strong) IBOutlet NSTextField *SubtitleField;
@property (nonatomic, strong) NSDictionary *InfoDictionary;
@property (nonatomic, strong) NSPopover *popover;

+ (id) cellFromNibWithInfo:(NSDictionary*)info;
- (IBAction)saveAsButtonClicked:(id)sender;

@end
