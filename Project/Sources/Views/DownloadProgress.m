//
//  DownloadProgress.m
//  StockPiler
//
//  Created by heavenly-awker on 29/01/14.
//  Copyright (c) 2014 tuck. All rights reserved.
//

#import "DownloadProgress.h"

@implementation DownloadProgress

- (void)drawRect:(NSRect)dirtyRect {
    //Draw outline
    NSBezierPath *border = [NSBezierPath bezierPathWithRoundedRect:dirtyRect xRadius:5 yRadius:5];
    [border setLineWidth:5];
    [[NSColor lightGrayColor] setFill];
    [border fill];
    
    //Draw white space between outline and progress
    NSRect background = NSMakeRect(dirtyRect.origin.x + 1, dirtyRect.origin.x + 1, dirtyRect.size.width - 2, dirtyRect.size.height - 2);
    NSBezierPath *backgroundBezier = [NSBezierPath bezierPathWithRoundedRect:background xRadius:4 yRadius:4];
    [backgroundBezier setLineWidth:5];
    [[NSColor whiteColor] setFill];
    [backgroundBezier fill];
    
    //Draw progress
    if (progressValue < 3 && progressValue > 0) {
        progressValue = 2;
    }
    
    if (progressValue != 0) {
        NSRect progressContainer = NSMakeRect(background.origin.x + 1, background.origin.y + 1, background.size.width - 2, background.size.height - 2);
        double width = (progressValue/100) * (double)progressContainer.size.width;
        NSRect progress = NSMakeRect(progressContainer.origin.x, progressContainer.origin.y, (CGFloat)width, progressContainer.size.height);
        NSBezierPath *progressBezier = [NSBezierPath bezierPathWithRoundedRect:progress xRadius:3 yRadius:3];
        CGFloat startRed = 0.234375;
        CGFloat startGreen = 0.4726562;
        CGFloat startBlue = 0.828125;
        NSColor *startingColor = [NSColor colorWithDeviceRed: startRed green:startGreen blue:startBlue alpha:1.0];
        CGFloat endRed = 0.214843;
        CGFloat endGreen = 0.324218;
        CGFloat endBlue = 0.746093;
        NSColor *endingColor = [NSColor colorWithDeviceRed:endRed green:endGreen blue:endBlue alpha:0.5];
        NSGradient *progressGradient = [[NSGradient alloc] initWithStartingColor:startingColor endingColor:endingColor];
        [progressGradient drawInBezierPath:progressBezier angle:270];
    }
}

- (void)incrementBy:(double)doubleValue {
    progressValue = doubleValue;
    [self setNeedsDisplay:YES];
}

- (void)setProgressValue:(double)doubleValue {
    progressValue = doubleValue;
    [self setNeedsDisplay:YES];
}

- (double)doubleValue {
    return progressValue;
}

@end
