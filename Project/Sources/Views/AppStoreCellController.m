//
//  AppStoreCellController.m
//  StockPiler
//
//  Created by heavenly-awker on 23/12/13.
//  Copyright (c) 2013 tuck. All rights reserved.
//

#import "AppStoreCellController.h"
#import "LinkController.h"

@interface AppStoreCellController()

- (NSNumber*)downloadedSize;
- (NSNumber*)totalSize;
- (void)updateProgress;

@end

@implementation AppStoreCellController

@synthesize DownloadProgress = downloadProgress;
@synthesize SaveApp = saveApp;
@synthesize InfoDictionary = infoDictionary;
@synthesize popover = popover;
@synthesize SubtitleField = subtitleField;
@synthesize ImageView = imageView;

- (BOOL)canBecomeKeyView {
    return YES;
}

- (BOOL)needsPanelToBecomeKey {
    return YES;
}

- (NSNumber*)downloadedSize {
    NSString *filePath;
    if (infoDictionary) {
        filePath = [infoDictionary valueForKey:@"path"];
    } else {
        NSLog(@"WARNING 9002:No dictionary found");
    }
    if (filePath) {
        return [NSNumber numberWithUnsignedLongLong:[[[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil] fileSize]];
    } else {
        NSLog(@"WARNING 9001:No file to find size of");
    }
    return nil;
}
- (NSNumber*)totalSize {
    NSNumber *totalSize;
    if (infoDictionary) {
        totalSize = [infoDictionary valueForKey:@"size"];
    } else {
        NSLog(@"WARNING 9003: No dictionary found");
    }
     if (totalSize) {
        return totalSize;
    } else {
        NSLog(@"WARNING 9004: No size available");
    }
    return nil;
}

+ (id)cellFromNibWithInfo:(NSDictionary*)info;
{
    NSNib *nib = [[NSNib alloc] initWithNibNamed:@"AppStoreCell" bundle:[NSBundle mainBundle]];
    NSArray *topLevelObjects;
    if (![nib instantiateWithOwner:self topLevelObjects:&topLevelObjects]) {
        return nil;
    }
    AppStoreCellController *appStoreCell = nil;
    for (id topLevelObject in topLevelObjects) {
        if ([topLevelObject isKindOfClass:[AppStoreCellController class]]) {
            appStoreCell = topLevelObject;
            break;
        }
    }
    [appStoreCell setInfoDictionary:info];
    [[appStoreCell TitleField] setStringValue:[[appStoreCell InfoDictionary] valueForKey:@"title"]];
    [[appStoreCell SubtitleField] setStringValue:[[appStoreCell InfoDictionary] valueForKey:@"subtitle"]];
    [[appStoreCell ImageView] setImage:[[appStoreCell InfoDictionary] objectForKey:@"image"]];
    [appStoreCell updateProgress];
    if (![[LinkController sharedLinker] isThereALinkForAppWithTitle:[[appStoreCell InfoDictionary] valueForKey:@"title"]]) {
        NSRect oldButtonFrame = [[appStoreCell SaveApp] frame];
        CGFloat spaceAtRight = 360 - (oldButtonFrame.origin.x + oldButtonFrame.size.width);
        [[appStoreCell SaveApp] setTitle:@"Save"];
        [[appStoreCell SaveApp] sizeToFit];
        NSRect buttonFrame = [[appStoreCell SaveApp] frame];
        CGFloat newX = 360 - (spaceAtRight + buttonFrame.size.width);
        [[appStoreCell SaveApp] setFrame:NSMakeRect(newX, buttonFrame.origin.y, buttonFrame.size.width, buttonFrame.size.height)];
    }
    NSTimer *timer;
    timer = [NSTimer scheduledTimerWithTimeInterval: 1
                                             target: appStoreCell
                                           selector: @selector(updateProgress)
                                           userInfo: nil
                                            repeats: YES];
    return appStoreCell;
}

- (void)updateProgress {
    [downloadProgress setProgressValue:([[self downloadedSize] doubleValue] * 100) / [[self totalSize] doubleValue]];
}

- (IBAction)saveAsButtonClicked:(id)sender {
    if ([[saveApp title] isEqualToString:@"Don't Save"]) {
        [[LinkController sharedLinker] removeLinkForAppWithTitle:[infoDictionary valueForKey:@"title"]];
        NSRect oldButtonFrame = [saveApp frame];
        CGFloat spaceAtRight = 360 - (oldButtonFrame.origin.x + oldButtonFrame.size.width);
        [saveApp setTitle:@"Save"];
        [saveApp sizeToFit];
        NSRect buttonFrame = [saveApp frame];
        CGFloat newX = 360 - (spaceAtRight + buttonFrame.size.width);
        [saveApp setFrame:NSMakeRect(newX, buttonFrame.origin.y, buttonFrame.size.width, buttonFrame.size.height)];
    } else {
        [[LinkController sharedLinker] addLinkForAppWithTitle:[infoDictionary valueForKey:@"title"]];
        NSRect oldButtonFrame = [saveApp frame];
        CGFloat spaceAtRight = 360 - (oldButtonFrame.origin.x + oldButtonFrame.size.width);
        [saveApp setTitle:@"Don't Save"];
        [saveApp sizeToFit];
        NSRect buttonFrame = [saveApp frame];
        CGFloat newX = 360 - (spaceAtRight + buttonFrame.size.width);
        [saveApp setFrame:NSMakeRect(newX, buttonFrame.origin.y, buttonFrame.size.width, buttonFrame.size.height)];
    }
}

@end
